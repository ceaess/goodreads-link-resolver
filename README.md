# Goodreads Link Resolver
Based on https://github.com/mixmaxhq/giphy-example-link-resolver

This is an open source Mixmax Link Resolver.
See <http://developer.mixmax.com/docs/overview-link-resolvers> for more information about how to use this example code in Mixmax.

## Running locally

1. Install using `npm install`
2. Run using `npm start`

To simulate locally how Mixmax calls the resolver URL (to return HTML that goes into the email), run:

`curl https://localhost:9146/resolver\?url\=https%3A%2F%2Fwww.goodreads.com%2Fbook%2Fshow%2F28957268-queer --insecure`

## Why do we run it in https locally?

Mixmax slash command APIs are required to be served over https. This is because they are queried directly from the Mixmax client in the browser (using AJAX) that's running on an HTTPS domain. Browsers forbid AJAX requests from https domains to call http APIs, for security. So we must run an https server with a locally-signed certificate.

See [here](http://developer.mixmax.com/docs/integration-api-appendix#local-development-error-neterr_insecure_response) for how to fix the **ERR_INSECURE_RESPONSE** error that you might get in Chrome.
`

## Deploying to Heroku

Everything you need is here, just create a new node app on Heroku and push this repo up to it.
Described here: https://devcenter.heroku.com/categories/nodejs
