var request = require('request');
var _ = require('underscore');

// The API that returns the in-email representation.
module.exports = function(req, res) {
  var url = req.query.url.trim();

  // Goodreads book urls from clicking around in profiles
  // (there are many book urls, one implemented for now)
  // are in the format:
  // https://www.goodreads.com/book/show/<numeric book id>-<title-prefix>
  var matches = url.match(/\/([0-9]+)-/);
  if (!matches) {
    res.status(400).send('Invalid URL format');
    return;
  }

  var book_id = matches[1];
  var html = `
  <div id="gr_add_to_books">
      <div class="gr_cust om_each_container_">
        <img src="https://www.goodreads.com/images/atmb_add_book-70x25.png" />
      </div>
  </div>
  <script src="https://www.goodreads.com/book/add_to_books_widget_frame/`
    + book_id + '?atmb_widget%5Bbutton%5D=atmb_widget_1.png"></script>'
  res.json({
    body: html
  });
};
